#ifndef LAB3_INCLUDE_HW_L3_DOMAINLAYER_H_
#define LAB3_INCLUDE_HW_L3_DOMAINLAYER_H_
#include "hw/l4_InfrastructureLayer.h"

const int MIN_ID = 0;
const int MIN_ID_CLIENT = 0;
const int MIN_COEFF = 0;
const int MAX_COEFF = 100;
const int MIN_SUM = 0;
const int MAX_SUM = 10000000;

class Winner {
  bool _win;
  float _premia;
 public:
  Winner() {}
  explicit Winner(bool win, float premia=0);
  bool getWin() const;
  void setPremia(float premia);
  float getPremia();
};

class BetCollection : public ICollectable {
 protected:
  bool invariant() const;
 public:
  BetCollection() = delete;
  BetCollection(const BetCollection& p) {
	this->_id = p.getId();
	this->_id_client = p.getIdClient();
	this->_sum = p.getSum();
	this->_coefficient = p.getCoefficient();
  };
  BetCollection& operator=(const BetCollection& p) = delete;
  BetCollection(int id, int id_client, float coefficient, int sum);
  BetCollection(int id, int id_client, float coefficient, int sum, Winner winner);
  bool write(std::ostream& os) override;
  int getId() const;
  int getIdClient() const;
  float getCoefficient() const;
  int getSum() const;
  void setWinners(Winner winner);
  Winner& getWinner();
 private:
  int _id;
  int _id_client;
  float _coefficient;
  int _sum;
  Winner _winners;
};

class ItemCollector: public ACollector {
 public:
  virtual std::shared_ptr<ICollectable> read(std::istream& is) override;
  void addWinner(int index, Winner winner);
  void serPremia(int index);
};

#endif //LAB3_INCLUDE_HW_L3_DOMAINLAYER_H_
