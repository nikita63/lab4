#include "hw/l3_DomainLayer.h"

bool BetCollection::invariant() const {
  return _id > MIN_ID && _id_client > MIN_ID_CLIENT && _coefficient > MIN_COEFF && _coefficient <= MAX_COEFF && _sum > MIN_SUM && _sum <= MAX_SUM;
}
bool BetCollection::write(std::ostream& os) {
  writeNumber(os, _id);
  writeNumber(os, _id_client);
  writeNumber(os, _coefficient);
  writeNumber(os, _sum);
  writeNumber(os, _winners.getWin());
  writeNumber(os, _winners.getPremia());
  return os.good();
}
int BetCollection::getId() const {
  return _id;
}
int BetCollection::getIdClient() const {
  return _id_client;
}
float BetCollection::getCoefficient() const {
  return _coefficient;
}
int BetCollection::getSum() const {
  return _sum;
}
BetCollection::BetCollection(int id, int id_client, float coefficient, int sum): _id(id), _id_client(id_client), _coefficient(coefficient), _sum(sum) {
  assert(invariant());
}


void BetCollection::setWinners(Winner winner) {
	_winners = winner;
}
Winner &BetCollection::getWinner() {
  return _winners;
}
BetCollection::BetCollection(int id, int id_client, float coefficient, int sum, Winner winner): _id(id), _id_client(id_client), _coefficient(coefficient), _sum(sum), _winners(winner) {
  assert(invariant());
}

std::shared_ptr<ICollectable> ItemCollector::read(std::istream &is) {
  int id = readNumber<int>(is);
  int id_client = readNumber<int>(is);
  int coefficient = readNumber<float>(is);
  int sum = readNumber<int>(is);
  int win = readNumber<bool>(is);
  int premia = readNumber<float>(is);
  return std::make_shared<BetCollection>(id, id_client, coefficient, sum, Winner(win, premia));
}

void ItemCollector::addWinner(int index, Winner winner) {
  BetCollection & item = dynamic_cast<BetCollection &>(*getItem(index));
  item.setWinners(winner);
}
void ItemCollector::serPremia(int index) {
  BetCollection & item = dynamic_cast<BetCollection &>(*getItem(index));
  std::vector<BetCollection> current_bet{};
  for(size_t i = 0; i < getSize(); ++i) {
	const BetCollection & item_c = dynamic_cast<BetCollection &>(*getItem(i));
	if (!isRemoved(i) && item_c.getId() == item.getId()) {
	  current_bet.push_back(item_c);
	}
  }

  float Sr = 0;
  int sum = 0;
  for(const auto& bet:current_bet) {
	sum += bet.getSum();
  }
  Sr = (float)sum - ((float)sum * 0.1);

  for(size_t i = 0; i < getSize(); ++i) {
    BetCollection & item_c = dynamic_cast<BetCollection &>(*getItem(i));
	if (!isRemoved(i) && item_c.getId() == item.getId() && item_c.getWinner().getWin()) {
	  float premia = Sr * item_c.getSum()/sum;
	  item_c.getWinner().setPremia(premia);
	}
  }
}

Winner::Winner(bool win, float premia): _win(win), _premia(premia) {}

bool Winner::getWin() const {
  return _win;
}
void Winner::setPremia(float premia) {
	_premia = premia;
}
float Winner::getPremia() {
  return _premia;
}
